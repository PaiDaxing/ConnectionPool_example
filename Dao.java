package DBtools;

import com.CartGood;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Dao extends BaseDao{

    public void setStudent(String studentid, char task) throws SQLException {
        // 先查找这个学生id是不是已经注册过了。
        // 如果没有注册过，那就注册这个id和相应的task。
        String sql2 = "INSERT INTO student set studentId ='" + studentid + "', task ='" + task + "', loginTimes=1"
                + " on duplicate key update loginTimes=loginTimes+1;";
        executeUpdate(sql2);
    }

    public HashMap<String, String> goodinfo(String studentid, String goodid) {
        try {
            boolean flag = false;
            try {
                // 则增加一条这样的记录
                String sql2 = "INSERT INTO action set studentId ='" + studentid + "', goodid ='" + goodid
                        + "',action='click',aspiration='0'";
                 executeUpdate(sql2);
            } catch (SQLException e) {
                // 说明主键重复，已经添加过了不再执行下面的这一条sql
                flag = true;
            }
            if (flag == false) {
                String sql = "update good set clickNum =clickNum+1 where goodid ='" + goodid + "';";
                 executeUpdate(sql);
            }

            // 将商品的介绍信息返回给jsp页面
            HashMap<String, String> map = new HashMap<>();
            String sql3 = "select introduce,price, imgPath1,imgPath2,imgPath3,imgPath4 from good where goodid=" + goodid
                    + ";";
            ResultSet rs = executeQuery(sql3);
            // 这里rs中只有一个结果，因为goodid是主键；
            rs.next();
            map.put("introduce", rs.getString("introduce"));
            map.put("price", rs.getString("price"));
            map.put("imgPath1", rs.getString("imgPath1"));
            map.put("imgPath2", rs.getString("imgPath2"));
            map.put("imgPath3", rs.getString("imgPath3"));
            map.put("imgPath4", rs.getString("imgPath4"));
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addToCart(String studentid, String goodid) {
        try {
            boolean flag = false;
            try {
                // 生成一条行为记录
                String sql = "INSERT INTO action set studentId ='" + studentid + "', goodid ='" + goodid
                        + "',action='addCart',aspiration='0';";
                 executeUpdate(sql);
            } catch (SQLException e) {
                flag = true;
            }
            if (flag == false) {
                // 将对应商品的addCartNum加一
                String sql2 = "update good set addCartNum=addCartNum+1 where goodid='" + goodid + "'; ";
                 executeUpdate(sql2);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public ArrayList<CartGood> showCart(String studentid) {
        try {
            String sql = "select goodid from action where studentid ='" + studentid
                    + "' and action='addCart' and aspiration='0' ;"; //
            System.out.println(sql);
            ResultSet rs = executeQuery(sql);
            // while(rs.next()){
            // System.out.println(rs.getString(1));
            // }
            ArrayList<CartGood> goods = new ArrayList<>();
            for (int i = 0; rs.next(); i++) {
                // 获得商品的id
                String goodid = rs.getString(1);
                // 查询这个id
                String sql2 = "select introduce,price, imgPath1 from good where goodid=" + goodid + ";";
                ResultSet rs2 = executeQuery(sql2);
                // 这里rs中只有一个结果，因为goodid是主键；
                rs2.next();
                CartGood good = new CartGood();
                good.goodid = goodid;
                good.introduce = rs2.getString(1);
                good.price = rs2.getString(2);
                good.imgPath = rs2.getString(3);
                goods.add(good);
            }
            return goods;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public void submitCart(String studentid, String message) {
        try {
            String[] messages = message.split("A");
            String goodid = "";
            String aspiration = "";
            for (int i = 0; i < messages.length; i++) {
                String[] strs = messages[i].split("B");
                goodid = strs[0];
                aspiration = strs[1];
                String sql = "INSERT INTO action set studentId ='" + studentid + "', goodid ='" + goodid
                        + "',action='addCart',aspiration='" + aspiration + "' on duplicate key update aspiration='"
                        + aspiration + "' ;";
                 executeUpdate(sql);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void clear() {

        try {
            //good中点击数量清零
            String sql = "update good set clickNum='0',addCartNum='0' where goodId=goodId";
             executeUpdate(sql);
            //action表的 清空
            String sql2 = "delete from action where goodid=goodid";
            executeUpdate(sql2);
            //student表的 清空
            String sql3 = "delete from student where studentId=studentId";
             executeUpdate(sql3);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // public void setpath(){
    // for (int i = 1; i < 25; i++) {
    // String sql = "update good set imgPath1 ='glasses/" + i + "-1.png',
    // imgPath2 ='glasses/" + i + "-2.png',imgPath3 ='glasses/" + i +
    // "-3.png',imgPath4 ='glasses/" + i + "-4.png' where goodId ="+i+";";
    // executeUpdate(sql);
    // }
    // }

    public static void main(String[] args) {
        Dao db = new Dao();
        db.showCart("789");
        // db.setStudent("09011505232", 'A');
        // db.setpath();
    }
}
