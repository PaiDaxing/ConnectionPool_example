package DBtools;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDao {

    ConnectionPool connPool;

    public BaseDao() {
        // try {
        // // 初始化查找命名空间
        // Context ctx = new InitialContext();
        // // 参数java:/comp/env为固定路径
        // Context envContext;d
        // envContext = (Context) ctx.lookup("java:/comp/env");
        // // 参数jdbc/mysqlds为数据源和JNDI绑定的名字
        // DataSource ds = (DataSource) envContext.lookup("jdbc/test");
        // Connection conn = ds.getConnection();
        // } catch (NamingException | SQLException e) {
        // e.printStackTrace();
        // }
        connPool = new ConnectionPool("com.mysql.jdbc.Driver", "jdbc:mysql://XXX.XXX.XXX.XXX:3306/数据库名",
                "root", "password");
        try {
            connPool.createPool();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ResultSet executeQuery(String sql) {
        ConnectionPool.PooledConnection pConn = connPool.getConnection();
        ResultSet rs = null;
        try {
            Statement st = pConn.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = st.executeQuery(sql);
        } catch (SQLException e) {
            //假如这里的语句执行失败，那我们推测可能是因为连接本身除了问题,这时候我们重置连接，并且再次执行
            connPool.refreshConnection(pConn);
            try {
                Statement st = pConn.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = st.executeQuery(sql);
            } catch (SQLException e1) {
                e1.printStackTrace();
                //再次失败，则放弃执行
            }

        }
        connPool.returnConnection(pConn);
        return rs;
    }

    void executeUpdate(String sql) throws SQLException {
        ConnectionPool.PooledConnection pConn = connPool.getConnection();
        try {
            Statement st = pConn.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            st.executeUpdate(sql);
        } catch (SQLException e) {
            //假如这里的语句执行失败，那我们推测可能是因为连接本身除了问题,这时候我们重置连接，并且再次执行
            connPool.refreshConnection(pConn);
            try {
                Statement st = pConn.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                st.executeUpdate(sql);
            } catch (SQLException e1) {
                e1.printStackTrace();
                //再次失败，则放弃执行
                throw e1;
            }
        }
        connPool.returnConnection(pConn);
    }

}
