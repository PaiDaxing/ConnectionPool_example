package DBtools.backUp;

import java.util.LinkedList;

/**
 * 这里构建一个线程安全的链表
 * 线程安全的链表有两种构建方法
 * 锁住整个链表。
 * 使用交替锁（hand-over-hand locking）。只锁住链表的一部分，链表没有被锁住的部分自由访问。
 *
 * 交替锁的具体实现示例，
 *
 * 这里先演示第一种方法，
 * http://baijiahao.baidu.com/s?id=1592849044084994070&wfr=spider&for=pc
 */
 class SynLinkedlist<E> extends LinkedList<E> {
    @Override
    public synchronized E get(int index) {
        return super.get(index);
    }

    @Override
    public synchronized boolean add(E e) {
        return super.add(e);
    }

    @Override
    public synchronized void add(int index, E element) {
        super.add(index, element);
    }

    @Override
    public synchronized E remove(int index) {
        return super.remove(index);
    }

    @Override
    public synchronized int size() {
        return super.size();
    }

    @Override
    public synchronized E poll() {
        return super.poll();
    }
}
