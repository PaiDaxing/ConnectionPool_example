package DBtools;

import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 这是一个分段锁机制下的LinkedList，介于之前版本的list使用的整体加锁的方法，并发效果不高，这里做出改良版的建议
 *
 * @param <E>
 */
public class SynLinkedList<E> {
    int size;
    Node<E> itrPoint;
    Iterator<E> iterator = new Iterator<E>() {
        //为了满足迭代器遍历条件，需要实现这两个函数，
        /**
         *  检查是否有下一个迭代点
          */
        @Override
        public boolean hasNext() {
            if(itrPoint.next!=lastNext){
                //说明后面还有节点
                return true;
            }
            return false;
        }

        /**
         * //迭代指针位置加一，并且返回迭代指针所指的节点的内容
         * @return
         */
        @Override
        public E next() {
            itrPoint=itrPoint.next;
            return itrPoint.item;
        }
    };

    /**
     * 返回迭代器
     * @return
     */
    Iterator<E> iterator(){
        //把迭代器指针重新置位
        itrPoint=firstPrev;
        return iterator;
    }

    //设置两个特殊的节点,分别表示链表的头部和尾部，这两个节点在链表中并不存在，但却可以加锁
    private transient Node<E> firstPrev;
    private transient Node<E> lastNext;

    public SynLinkedList() {
        super();
        firstPrev = new Node<>(null, null, lastNext);
        lastNext = new Node<>(firstPrev, null, null);
    }

    public static void main(String[] args) {
        SynLinkedList<Integer> list = new SynLinkedList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);

        }
        for (int i = 0; i < 10; i++) {
            System.out.println("now size=" + list.size());
            System.out.println(list.poll());
        }
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    public int size() {
        return size;
    }

    public void add(int index, E element) {
        checkElementIndex(index);
        linkBefore(element, node(index));
    }

    public boolean add(E element) {
        linkBefore(element, lastNext);
        return true;
    }

    /**
     * 取出但不移除相应的元素
     *
     * @param index
     * @return
     */
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    /**
     * 取出并且清除第一个元素,如果链表为空，就返回一个null
     */
    public E poll() {
        return remove(0);
    }

    /**
     * 此函数是线程安全的
     *
     * @param e
     * @param succ
     */
    void linkBefore(E e, Node<E> succ) {
        final Node<E> newNode = new Node<>(succ.prev, e, succ);
        succ.lock();
        succ.prev.lock();
        newNode.lock();

        succ.prev.next = newNode;
        succ.prev = newNode;
        size++;
        //开锁
        succ.lock();
        succ.prev.prev.lock();
        newNode.unlock();
    }

    /**
     * 此函数是线程安全的
     *
     * @param x
     * @return
     */
    E unlink(Node<E> x) {
        x.prev.lock();
        x.next.lock();
        x.lock();

        x.next.prev = x.prev;
        x.prev.next = x.next;
        size--;

        x.prev.unlock();
        x.next.unlock();
        x.unlock();

        return x.item;
    }

    public E remove(int index) {
        checkElementIndex(index);
        return unlink(node(index));
    }

    /**
     * 从链表的头部开始查找这个元素，有则移除,返回true，没有则返回false
     *
     * @param o
     * @return
     */

    public boolean remove(Object o) {
        if (unlink(node((E) o)) != null) {
            //说明存在并且删除成功，返回true
            return true;
        } else {
            //说明不存在，返回false
            return false;
        }


    }

    /**
     * 从链表的尾部开始查找这个元素，查到后移除，没有查到则返回null
     *
     * @param element
     * @return
     */
    public E removeRev(E element) {
        return unlink(nodeRev(element));
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    /**
     * 根据index序号，查找到相应的node，
     *
     * @param index
     * @return 返回node
     */
    Node<E> node(int index) {
        // assert isElementIndex(index);
        if (index < (size >> 1)) {
            Node<E> x = firstPrev.next;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = lastNext.prev;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    /**
     * 这个方法是在倒序查找元素
     *
     * @param element
     * @return
     */
    Node<E> node(E element) {
        Node node = firstPrev;
        for (int i = 0; i < size; i++) {
            node = node.next;
            //这里要求的是不但内容相同，内存地址也相同，
            if (element == node.item) {
                return node;
            }
        }
        return null;
    }

    /**
     * 这个方法是在倒序查找元素
     *
     * @param element
     * @return
     */
    Node<E> nodeRev(E element) {
        Node node = lastNext;
        for (int i = size - 1; i < 0; i--) {
            node = node.prev;
            //这里要求的是不但内容相同，内存地址也相同，
            if (element == node.item) {
                return node;
            }
        }
        return null;
    }

    //@Test
    public void show() {
        SynLinkedList<Integer> list = new SynLinkedList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        for (int i = 0; i < 11; i++) {
            System.out.println(list.poll());
        }
    }

    /**
     * 这里我们让node继承可重入锁，方便后来加锁和开锁操作
     *
     * @param <E>
     */
    // 静态内部类只能访问外部类的静态成员
    private static class Node<E> extends ReentrantLock {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}
