package DBtools;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 这里我们创建一个守护线程，用来在连接池闲置的时候减少连接池的保持连接的数量
 */
public class Deamon {
    //连接池中当前的连接数量
    int currentNum;
    //连接池中当前正忙的连接数量
    int currentBusyNum;
    //和上一个时间点相比，放回连接多少次；
    int releaseNum;
    //和上一个时间点相比，取出连接多少次；
    int occupyNum;

    ConnectionPool connPool;
    //执行周期
    int mSeconds = 2000;
    Timer timer = new Timer(true);
    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            refreshStatus();
            //这里我们每过一段时间，就执行一次连接池情况统计命令，如果符合条件，那么我们会关闭部分连接。
            int index = countIndex();
            try {
                if (index > 0) {
                    //增加一部分连接
                    connPool.createConnections(index);
                } else {
                    //释放一部分连接
                    connPool.releaseConnections(index, false);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    };

    public Deamon(ConnectionPool connPool) {
        this.connPool = connPool;
    }

    public void start() {
        timer.schedule(task, 0, mSeconds);
    }

    public void refreshStatus() {
        currentNum = connPool.pConnections.size();
        Iterator<ConnectionPool.PooledConnection> it = connPool.pConnections.iterator();
        currentBusyNum = 0;
        while (it.hasNext()) {
            ConnectionPool.PooledConnection pConn = it.next();
            if (pConn.isBusy()) {
                currentBusyNum++;
            }
        }
        releaseNum = connPool.releaseNum;
        occupyNum = connPool.occupyNum;
        connPool.occupyNum = 0;
        connPool.releaseNum = 0;
    }


    public int countIndex() {
        //FIXME 这里依据使用场景的不同，所使用的连接数控制算法可能也会不同，可以根据需要进行设计
        //这里给出一种示例：

        //空闲连接数=当前连接数量-当前正忙的连接数量
        int free = currentNum - currentBusyNum;
        //繁忙率
        int busyRate = currentBusyNum / currentNum;
        //系统活跃趋势(即连接数是趋向于增加，还是减少)
        double ActiveTrend = occupyNum / (releaseNum + occupyNum) - 0.5;
        //连接吞吐率(注意这个值可能大于1)
        double vitality = (occupyNum + releaseNum) / currentBusyNum * 2;
        //需要关闭或者开启的连接数量（index为正，则是需要开启）
        int index = (int) (currentNum * ((busyRate - 0.8) * currentNum + ActiveTrend));
        return index;
    }

//    //开始执行任务,第一个参数是任务,第二个是延迟时间,第三个是每隔多长时间执行一次
//    public static void main(String[] args) throws InterruptedException {
//        Deamon deamon =new Deamon();
//        System.out.println("show");
//        deamon.timer.schedule(deamon.task, 0, 1000);
//        for (int i = 0; i <1000000 ; i++) {
//            Thread.sleep(1000);
//        }
//    }
}
