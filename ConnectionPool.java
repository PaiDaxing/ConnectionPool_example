package DBtools;

import java.sql.*;

/**
 * 一个效果非常不错的JAVA数据库连接池.
 * 在原基础上进行了改进，添加了守护线程，控制链接数量，
 * 另外改用线程安全的链表进行连接管理（空闲连接都会放入链表头部，繁忙连接都会被放入链表尾部），优化查找速度
 */
public class ConnectionPool {
    private String jdbcDriver = "com.mysql.jdbc.Driver"; // 数据库驱动
    private String dbUrl = "jdbc:mysql://localhost:3306/test"; // 数据 URL
    private String dbUsername = "root"; // 数据库用户名
    private String dbPassword = "root"; // 数据库用户密码
    private String testTable = ""; // 测试连接是否可用的测试表名，默认没有测试表
    private int initialConnections = 15; // 连接池的初始大小
    private int incrementalConnections = 5; // 连接池自动增加的大小
    private int maxConnections = 100; // 连接池最大的大小
    SynLinkedList<PooledConnection> pConnections = null; // 存放连接池中数据库连接的向量 , 初始时为 null

    //守护线程
    Deamon deamon = new Deamon(this);

    //FIXME 需要解决too many connections的问题
    //连接池中当前的连接数量
    int currentNum;
    //连接池中当前正忙的连接数量
    int currentBusyNum;
    //和上一个时间点相比，放回连接多少次；
    int releaseNum;
    //和上一个时间点相比，取出连接多少次；
    int occupyNum;


    public ConnectionPool(String jdbcDriver, String dbUrl, String dbUsername, String dbPassword) {
        this.jdbcDriver = jdbcDriver;
        this.dbUrl = dbUrl;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
    }

    public int getInitialConnections() {
        return this.initialConnections;
    }

    public int getIncrementalConnections() {
        return this.incrementalConnections;
    }

    public int getMaxConnections() {
        return this.maxConnections;
    }

    public String getTestTable() {
        return this.testTable;
    }

    public synchronized void createPool() throws Exception {
        // 确保连接池没有创建
        // 假如连接池己经创建了，保存连接的向量 pConnections 不会为空
        if (pConnections != null) {
            return; // 假如己经创建，则返回
        }
        //开启守护线程
        deamon.timer.schedule(deamon.task, 0, 1000);
        // 实例化 JDBC Driver 中指定的驱动类实例
        Driver driver = (Driver) (Class.forName(this.jdbcDriver).newInstance());
        DriverManager.registerDriver(driver); // 注册 JDBC 驱动程序
        // 创建保存连接的向量 , 初始时有 0 个元素
        pConnections = new SynLinkedList();
        // 根据 initialConnections 中设置的值，创建连接。
        createConnections(this.initialConnections);
        System.out.println(" 数据库连接池创建成功！ ");
    }

    synchronized void createConnections(int numConnections) throws SQLException {
        // 循环创建指定数目的数据库连接
        for (int x = 0; x < numConnections; x++) {
            // 是否连接池中的数据库连接的数量己经达到最大？最大值由类成员 maxConnections
            // 指出，假如 maxConnections 为 0 或负数，表示连接数量没有限制。
            // 假如连接数己经达到最大，即退出。
            if (this.maxConnections > 0 && this.pConnections.size() >= this.maxConnections) {
                break;
            }
            // add a new PooledConnection object to pConnections vector
            // 增加一个连接到连接池中（向量 pConnections 中）
            try {
                //新建一个连接，添加到链表的头部
                pConnections.add(0, new PooledConnection(newConnection()));
            } catch (SQLException e) {
                System.out.println(" 创建数据库连接失败！ " + e.getMessage());
                throw new SQLException();
            }
            System.out.println(" 数据库连接己创建 ......");
        }
    }

    private Connection newConnection() throws SQLException {
        // 创建一个数据库连接
        Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        // 假如这是第一次创建数据库连接，即检查数据库，获得此数据库答应支持的
        // 最大客户连接数目
        // pConnections.size()==0 表示目前没有连接己被创建
        if (pConnections.size() == 0) {
            //FIXME 这里应该是每次扩容的时候都要检查
            DatabaseMetaData metaData = conn.getMetaData();
            int driverMaxConnections = metaData.getMaxConnections();
            // 数据库返回的 driverMaxConnections 若为 0 ，表示此数据库没有最大
            // 连接限制，或数据库的最大连接限制不知道
            // driverMaxConnections 为返回的一个整数，表示此数据库答应客户连接的数目
            // 假如连接池中设置的最大连接数量大于数据库答应的连接数目 , 则置连接池的最大
            // 连接数目为数据库答应的最大数目减去3，因为要预留一部分给数据库远程客户端
            if (driverMaxConnections > 0 && this.maxConnections > driverMaxConnections) {
                this.maxConnections = driverMaxConnections - 3;
            }
        }
        return conn; // 返回创建的新的数据库连接
    }

    /**
     * 取出一个可用，没有被关闭的连接，并返回，如果没有，就返回null
     *
     * @return
     * @throws SQLException
     */
    private synchronized PooledConnection getFreeConnection() throws SQLException {
        PooledConnection pConn = null;
        // 获得连接池向量中所有的对象
        for (int i = 0; i < 3; i++) {
            synchronized (pConnections) {
                pConn = pConnections.get(0);
                while (pConn.getConnection().isClosed()) {
                    pConnections.remove(0);
                }
                if (pConn.isBusy()) {
                    // 假如目前连接池中没有可用的连接
                    // 创建一些连接
                    new Exception("守护线程Deamon的算法可能还需要优化，出现连接不够用的情况了").printStackTrace();
                    createConnections(incrementalConnections);
                } else {
                    // 假如此对象不忙，则获得它的数据库连接并把它设为忙
                    pConn.setBusy(true);
                    //把这个连接移动到链表的尾部
                    pConnections.remove(0);
                    pConnections.add(pConn);
                    // FIXME 还没有测试此连接是否可用，另外，注意setBusy应该在什么时候添加
                    return pConn; // 返回找到的可用连接
                }
            }
        }
        new Exception("连接数量已经达到上限，且没有空闲的连接，无法返回可用连接").printStackTrace();
        return null;
    }


    /**
     * 找到一个可用的连接，如果没有可用连接，那就等待，最多等待三次
     *
     * @return
     */
    public synchronized PooledConnection getConnection() {
        try {
            //计数器加一
            occupyNum++;
            // 确保连接池己被创建
            if (pConnections == null) {
                return null; // 连接池还没创建，则返回 null
            }
            PooledConnection pConn = getFreeConnection(); // 获得一个可用的数据库连接
            // 假如目前没有可以使用的连接，即所有的连接都在使用中
            for (int i = 0; pConn == null; i++) {
                wait(2000); // 等 3 秒
                // 重新再试，直到获得可用的连接，假如
                // getFreeConnection() 返回的为 null
                // 则表明创建一批连接后也不可获得可用连接
                pConn = getFreeConnection();
                if (i >= 3) {
                    new Exception("连接池已满或正忙，无法找到可用连接").printStackTrace();
                    break;
                }
            }
            return pConn; // 返回获得的可用的连接
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public synchronized void returnConnection(PooledConnection pConn) {
        // 确保连接池存在，假如连接没有创建（不存在），直接返回
        if (pConnections == null) {
            System.err.println(" 连接池不存在，无法返回此连接到连接池中 !");
            return;
        }
        //计数器加一
        releaseNum++;
        pConn.setBusy(false);
        //把这个连接移动到链表的头部
        pConnections.remove(pConn);
        pConnections.add(0, pConn);
    }

    /**
     * 重置连接池，原理是先关闭，然后在新建一个和这个连接数量相同的连接池
     *
     * @throws SQLException
     */
    public synchronized void refreshConnections() throws Exception {
        closeConnectionPool();
        //FIXME 这里要重建一个相同大小的
        createPool();
    }

    /**
     * 重置某一个连接
     *
     * @throws Exception
     */
    public synchronized void refreshConnection(PooledConnection pConn) {
        try {
            pConn.refreshConnection(newConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放指定数目的连接（优先释放空闲连接），这里分为软释放和硬释放（硬释放指的是就算连接处于busy状态，也强制释放）
     *
     * @param index
     */
    synchronized void releaseConnections(int index, boolean hard) {
        PooledConnection pConn = null;
        for (int i = 0; i < index; i++) {
            if ((pConn = pConnections.get(0)) != null) {
                if (!hard && pConn.isBusy()) {
                    //说明守护线程出现问题，报出异常，并且停止释放
                    new Exception("守护线程Deamon存在问题，已经停止释放").printStackTrace();
                    return;
                }
                pConn.closeConnection();
            }
        }
    }

    /**
     * 关闭整个连接池，将每一个连接安全销毁（如果这个连接正在用，就等待3秒，再销毁它）
     *
     * @throws SQLException
     */
    public synchronized void closeConnectionPool() throws SQLException {
        // 确保连接池存在，假如不存在，返回
        if (pConnections == null) {
            System.err.println(" 连接池不存在，无法关闭 !");
            return;
        }
        PooledConnection pConn = null;
        //poll函数表示获取并移除此列表的第一个元素
        while ((pConn = pConnections.poll()) != null) {
            pConn.closeConnection();
        }
        // 置连接池为空
        pConnections = null;
    }

    private void wait(int mSeconds) {
        try {
            Thread.sleep(mSeconds);
        } catch (InterruptedException e) {
        }
    }

    class PooledConnection {
        public Connection connection = null; // 数据库连接
        private boolean busy = false; // 此连接是否正在使用的标志，默认没有正在使用
        // 构造函数，根据一个 Connection 构告一个 PooledConnection 对象

        public PooledConnection(Connection connection) {
            this.connection = connection;
        }

        /**
         * 重置这个连接
         */
        public void refreshConnection(Connection newConnection) {
            closeConnection();
            connection = newConnection;
        }

        public void closeConnection() {
            ConnectionPool.this.pConnections.poll();
            for (int i = 0; busy; i++) {
                wait(3000); // 等 3 秒
                if (i >= 3) {
                    new Exception("有一个连接未响应，已经被强制关闭").printStackTrace();
                    break;
                }
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        public Connection getConnection() {
            return connection;
        }

        // 设置此对象的，连接
        public void setConnection(Connection connection) {
            this.connection = connection;
        }

        // 获得对象连接是否忙
        public boolean isBusy() {
            return busy;
        }

        // 设置对象的连接正在忙
        public void setBusy(boolean busy) {
            this.busy = busy;
        }

        //等待相应的秒数
        private void wait(int mSeconds) {
            try {
                Thread.sleep(mSeconds);
            } catch (InterruptedException e) {
            }
        }
    }

//    public static void main(String[] args) {
//        ConnectionPool connPool = new ConnectionPool("com.mysql.jdbc.Driver",
//                "jdbc:mysql://139.199.71.19:3306/fictitious_mall", "root", "paidaxing1234");
//        try {
//            connPool.createPool();
//        } catch (Exception ex1) {
//            ex1.printStackTrace();
//        }
//
//        // connPool.refreshConnections();
//        for (int i = 0; i < 46; i++) {
//            Connection conn = connPool.getConnection();
//            System.out.println(i + "-----" + conn);
//            connPool.returnConnection(conn);
//        }
//
//    }
}